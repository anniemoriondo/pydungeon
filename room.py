from prop import Prop
class Room():
    """A room in a dungeoncrawler."""
    def __init__(self, name, description=None):
        self.name = name
        self.title = name.title()
        self.description = "\nYou are in the " + self.name + "."
        
        # We'll need to store the long description in a text file.
        no_spaces = self.name.replace(" ", "_") 
        self.long_desc = "rooms/" + no_spaces + ".txt"

        # Ensure that there's at least a file there. Otherwise this gets messy.
        with open(self.long_desc, 'a') as file_object:
            file_object.write("")

        self.neighbors = (None, None, None, None, None,
            None, None, None, None, None)
        self.props = []
        self.visited = False



    def populate_map(self, nr=None, ner= None, er=None, ser = None, sr=None,
        swr=None, wr=None, nwr=None, ur=None, dr=None):
        """Populate a tuple with neighboring rooms."""
        self.neighbors = (nr, ner, er, ser, sr, swr, wr, nwr, ur, dr)
        # 0 - north / 1 - northeast / 2 - east / 3 - southeast / 4 - south
        # 5 - southwest / 6 - west / 7 - northwest / 8 - up / 9 - down

    def add_props(self, new_prop):
        self.props.append(new_prop)

    def list_props(self):
        for prop in self.props:
            print("There is " + prop.desc + " here.")
