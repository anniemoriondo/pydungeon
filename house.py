from room import Room
from prop import Prop

foyer = Room("foyer")
dining_room = Room("dining room")
living_room = Room("living room")
landing = Room("landing")
study = Room("study")
u_bathroom = Room("upstairs bathroom")
guest_room = Room("guest room")
bedroom = Room("bedroom")
hallway = Room("hallway")
d_bathroom = Room("downstairs bathroom")
kitchen = Room("kitchen")
mudroom = Room("mud room")
sunroom = Room("sun room")
basement = Room("basement")
boiler_room = Room("boiler_room")
workshop = Room("workshop")
garage = Room("garage")

puzzle = Prop("jigsaw puzzle")
puzzle.add_aliases(["puzzle", "jigsaw"])

apple = Prop("apple")
coaster = Prop("coaster")

foyer.populate_map(nr=dining_room, wr=landing, sr=living_room, ur=landing)
    #going east from the foyer gets you outside
dining_room.populate_map(sr=foyer, wr=kitchen)
living_room.populate_map(nr = foyer, wr=hallway)
landing.populate_map(nr=guest_room, er=foyer, sr=study, wr=u_bathroom, dr=foyer)
study.populate_map(nr=landing)
u_bathroom.populate_map(er=landing)
guest_room.populate_map(sr=landing)
bedroom.populate_map(nr=hallway)
hallway.populate_map(nr=kitchen, er=basement, sr=bedroom, wr=d_bathroom,
    dr=basement)
d_bathroom.populate_map(er=hallway)
kitchen.populate_map(nr=mudroom, er=dining_room, sr=hallway)
mudroom.populate_map(nr=sunroom, sr=kitchen) # going east gets you outside
sunroom.populate_map(sr=mudroom) #going west gets you outside
basement.populate_map(nr=boiler_room, wr=hallway, ur=hallway)
boiler_room.populate_map(wr=workshop, sr=basement)
workshop.populate_map(er=boiler_room, sr=garage)
garage.populate_map(nr=workshop) #going south gets you outside

dining_room.add_props(puzzle)
kitchen.add_props(apple)
living_room.add_props(coaster)