class Prop():
    """A prop for use in a dungeoncrawler."""
    def __init__(self, name):
        self.name = name.lower()
        self.aliases = [self.name]
        self.article = "a"

        initial = self.name[0]
        if initial in ['a', 'e', 'i', 'o', 'u']:
            self.article = "an"
        
        self.desc = self.article + " " + self.name

    def add_aliases(self, altnames):
        for alias in altnames:
            self.aliases.append(alias)

