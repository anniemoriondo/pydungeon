from house import *
import text_parser as tp


class Pydungeon():
    """A Python-based dungeoncrawler game."""
    def __init__(self):
        self.game_active = True
        self.current_room = foyer
        self.inventory = []
        self.intro = "text/intro.txt"
        self.help = "text/help.txt"

    def go_to_room(self, index):
        """Go to a room as indicated by a compass direction."""
        if self.current_room.neighbors[index]:
            self.current_room = self.current_room.neighbors[index]
        else:
            print("You cannot go that way.")

    def show_text(self, location):
        """Show boilerplate or descriptive text."""
        with open(location) as file_object:
            for line in file_object:
                print(line.rstrip())

    def show_surroundings(self):
        """Show the full description of a room if unfamiliar or if desired."""
        self.show_text(self.current_room.long_desc)


    def show_inventory(self):
        """Show all items in the player's inventory."""
        if self.inventory:
            print("You have the following items:")
            for item in self.inventory:
                print("\t" + item.desc)
        else:
            print("You are not carrying anything.")

    def play_game(self):
        """Play the game."""
        self.show_text(self.intro)
        while self.game_active:
            if(self.current_room.visited):
                print(self.current_room.description)
            else:
                self.show_surroundings()
            self.current_room.list_props()
            self.current_room.visited = True
            action_text = input("> ").lower()
            tp.parse_text(action_text, self)


my_game = Pydungeon()

my_game.play_game()
