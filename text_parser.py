from house import *

# Commonly used words stored so we can quickly look through them.
take_verbs = ["take", "get", "grab"]
drop_verbs = ["drop", "leave", "put"]
go_verbs = ["go", "travel", "walk"]

everything_words = ["all", "everything", "items"]
inventory_words = ["inventory", "inv", "i"]

n = ('north', 'n')
ne = ('northeast', 'ne')
e = ('east', 'e')
se = ('southeast', 'se')
s = ('south', 's')
sw = ('southwest', 'sw')
w = ('west', 'w')
nw = ('northwest', 'nw')
u = ('up', 'u')
d = ('down', 'd')
directionals = (n, ne, e, se, s, sw, w, nw, u, d)

def parse_text(input, game):
    """Parse a player's textual input to determine an action."""
    text = input.lower().strip()
    # oh my god we need regular expressions

    # Quit if requested.
    if text == "quit":
        game.game_active = False
        return
    
    # Show help if requested.
    if text == "help":
        game.show_text(game.help)
        return

    # Give full description of surroundings if requested.
    if text == "look":
        game.show_surroundings()
        return

    # Show inventory if requested.
    if text in inventory_words:
        game.show_inventory()
        return
    
    # Check whether a prop is being taken or dropped.
    if take_prop(text, game):
        return
    if drop_prop(text, game):
        return

    # Trim "go" and other locomotion words so that the direction is exposed.
    split = split_word(text)
    if split and split[0] in go_verbs:
        text = split[1]

    # Check for a direction and go there.
    room_index = get_room_direction(text)
    if room_index != -1:
        game.go_to_room(room_index)
        return

    

    # If all else fails...
    print("I'm sorry, you'll need to rephrase that.")

def split_word(text):
    """Find the next word in user input."""
    space = text.find(" ")
    if space == -1:
        return None
    else:
        return (text[0:space], text[space+1:len(text)])


def take_prop(text, game):
    """Take a prop from the current room."""
    # Validate that the player entered a take command.
    split = split_word(text)
    if split and split[0] in take_verbs:
        text = split[1]
    else:
        return False

    # Take all items if requested.
    if text in everything_words:
        while game.current_room.props:
            current_prop = game.current_room.props.pop()
            print("You now have " + current_prop.desc + ".")
            game.inventory.append(current_prop)
        return True

    # Take only one item.
    for prop in game.current_room.props:
        if text in prop.aliases:
            game.current_room.props.remove(prop)
            game.inventory.append(prop)
            print("You now have " + prop.desc + ".")
            return True

    # If an invalid take command is issued, inform the player.
    print("There doesn't seem to be one of those here.")
    return True

def drop_prop(text, game):
    """Drop a prop from your inventory into the current room."""
    # Validate that the player entered a drop command.
    split = split_word(text)
    if split and split[0] in drop_verbs:
        text = split[1]
    else:
        return False

    # Drop all items if requested.
    if text in everything_words:
        while game.inventory:
            current_prop = game.inventory.pop()
            print("You dropped " + current_prop.desc + ".")
            game.current_room.props.append(current_prop)
        return True

    # Drop only one item.
    for prop in game.inventory:
        if text in prop.aliases:
            game.inventory.remove(prop)
            game.current_room.props.append(prop)
            print("You dropped " + prop.desc + ".")
            return True

    # If an invalid drop command is issued, inform the player        
    print("You don't seem to have one of those.")
    return True


def get_room_direction(text):
    """Interpret text corresponding to a compass direction."""
    for value in range(0,10):
        if text in directionals[value]:
            return value
    return -1
